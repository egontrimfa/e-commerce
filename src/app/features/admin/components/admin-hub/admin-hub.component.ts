import { Component, OnInit } from '@angular/core';
import { UserInterface } from 'src/app/core/interfaces/auth';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-admin-hub',
  templateUrl: './admin-hub.component.html',
  styleUrls: ['./admin-hub.component.scss']
})
export class AdminHubComponent implements OnInit {
  public user: UserInterface = {} as UserInterface
  public isAdminSidenavOpened = false

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.user = this.authService.getUser()
  }

  toggleSidenav(): void {
    this.isAdminSidenavOpened = !this.isAdminSidenavOpened
  }
}
