import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable, startWith } from 'rxjs';
import { PriceInterface, ProductInterface, ProductTypeInterface } from 'src/app/shared/interfaces/product';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent extends SubscribeHelperComponent implements OnInit {
  public formGroup: FormGroup = {} as FormGroup
  private products: ProductInterface[] = []
  public filteredProducts: Observable<ProductInterface[] | null> | undefined
  public productType: ProductTypeInterface | null = null
  private productTypeSKU: String | null = null

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private productService: ProductService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    super()

    this.productTypeSKU = this.route.snapshot.paramMap.get('id')

    this.formGroup = this.formBuilder.group({
      parentSKU: ['', Validators.required],
      description: ['', [Validators.required, Validators.maxLength(256)]],
      stock: ['', [Validators.required, Validators.min(1), Validators.max(999)]],
      available: [true],
      price_original: ['', [Validators.min(0.01), Validators.max(9999.99)]],
      price_discount: ['', [Validators.min(0.01), Validators.max(9999.99)]],
      price_currency: ['', Validators.maxLength(5)],
      dimension_width: ['', [Validators.min(0.01), Validators.max(99.99)]],
      dimension_height: ['', [Validators.min(0.01), Validators.max(99.99)]],
      dimension_length: ['', [Validators.min(0.01), Validators.max(99.99)]],
      dimension_unit: ['', Validators.maxLength(30)],
    })
  }

  // Getters
  get parentSKU(): AbstractControl | null { return this.formGroup.get('parentSKU') }
  get description(): AbstractControl | null { return this.formGroup.get('description') }
  get stock(): AbstractControl | null { return this.formGroup.get('stock') }
  get available(): AbstractControl | null { return this.formGroup.get('available') }
  get price_original(): AbstractControl | null { return this.formGroup.get('price_original') }
  get price_discount(): AbstractControl | null { return this.formGroup.get('price_discount') }
  get price_currency(): AbstractControl | null { return this.formGroup.get('price_currency') }
  get dimension_width(): AbstractControl | null { return this.formGroup.get('dimension_width') }
  get dimension_height(): AbstractControl | null { return this.formGroup.get('dimension_height') }
  get dimension_length(): AbstractControl | null { return this.formGroup.get('dimension_length') }
  get dimension_unit(): AbstractControl | null { return this.formGroup.get('dimension_unit') }

  ngOnInit(): void {
    this.addSubscription(this.productService.getProducts().subscribe(
      (products) => {
        this.products = products

        this.filteredProducts = this.parentSKU?.valueChanges.pipe(
          startWith(''),
          map(value => (typeof value === 'string' ? value : value.title)),
          map(title => (title ? this.filter(title) : this.products.slice()))
        )
      }
    ))

    if(this.productTypeSKU) {
      this.addSubscription(this.productService.getProductTypeByID(this.productTypeSKU).subscribe(
        (productType) => {
          this.productType = productType
  
          const parent = this.products.find(product => product.parentSKU === this.productType?.parent_sku)
          if (parent) {
            this.parentSKU?.setValue(parent)
          }
  
          this.description?.setValue(this.productType.description)
          this.stock?.setValue(this.productType.stock)
          if (this.productType.prices.length) {
            this.price_original?.setValue(this.productType.prices[0].original)
            this.price_discount?.setValue(this.productType.prices[0].discount)
            this.price_currency?.setValue(this.productType.prices[0].currency)
          }
          if (this.productType.dimension) {
            this.dimension_width?.setValue(this.productType.dimension.width)
            this.dimension_height?.setValue(this.productType.dimension.height)
            this.dimension_length?.setValue(this.productType.dimension.length)
            this.dimension_unit?.setValue(this.productType.dimension.unit)
          }
          this.available?.setValue(this.productType.available)
        }
      ))
    }
  }

  display(product: ProductInterface) {
    return product && product.title ? String(product.title) : ''
  }

  filter(title: String): ProductInterface[] {
    return this.products.filter(product => product.title.toLowerCase().includes(title.toLowerCase()))
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      // this.formGroup.markAllAsTouched()
      return
    }

    let _productType: ProductTypeInterface = {
      parent_sku: (this.parentSKU?.value as ProductInterface).parentSKU,
      description: String(this.description?.value),
      stock: Number(this.stock?.value),
      prices: [
        {
          original: Number(this.price_original?.value),
          discount: Number(this.price_discount?.value),
          currency: String(this.price_currency?.value) 
        } as PriceInterface
      ],
      dimension: {
        width: Number(this.dimension_width?.value),
        height: Number(this.dimension_height?.value),
        length: Number(this.dimension_length?.value),
        unit: String(this.dimension_unit?.value),
      },
      images: [],
      available: Boolean(this.available?.value)
    }

    if (this.productType) {
      _productType = { ..._productType, sku: String(this.productTypeSKU) }

      this.addSubscription(this.productService.updateProductType(_productType).subscribe(
        () => {
          this.snackBar.open('Product updated successfully!', undefined, { duration: 2000 })
          this.router.navigate(['/admin'])
        }
      ))
    } else {
      this.addSubscription(this.productService.createProductType(_productType).subscribe(
        () => {
          this.snackBar.open('Product added successfully!', undefined, { duration: 2000 })
          this.router.navigate(['/admin'])
        }
      ))
    }
  }
}
