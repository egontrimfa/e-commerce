import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductTypeInterface } from 'src/app/shared/interfaces/product';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent extends SubscribeHelperComponent implements OnInit {
  public tableData: ProductTypeInterface[] = []
  public displayedColumns = [ 'sku', 'parentTitle', 'description', 'stock', 'available', 'options' ]

  @ViewChild(MatTable) table: MatTable<ProductTypeInterface> | undefined;

  constructor(
    private productService: ProductService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.addSubscription(this.productService.getProductTypes().subscribe(
      (productTypes) => {
        this.tableData = productTypes
      }
    ))
  }

  editProductType(sku: String): void {
    this.router.navigate(['/admin/product', sku])
  }

  deleteProductType(sku: String): void {
    this.addSubscription(this.productService.deleteProductTypeByID(sku).subscribe(
      () => {
        const entryID = this.tableData.findIndex(entry => entry.sku === sku)
        if (entryID) {
          this.tableData.splice(entryID, 1)
          this.table?.renderRows()
        }
      }
    ))
  }

}
