import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, map, Observable } from 'rxjs';
import { ProductInterface, ProductTypeInterface } from 'src/app/shared/interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private httpClient: HttpClient
  ) { }

  getProducts(): Observable<ProductInterface[]> {
    return this.httpClient.get<any[]>('http://172.19.15.128:30001/product/list').pipe(
      map(products => products.map(product => { return { parentSKU: product._id, title: product.title } as ProductInterface }))
    )
  }

  getProductTypes(): Observable<ProductTypeInterface[]> {
    return this.httpClient.get<any[]>('http://172.19.15.128:30001/product/type/list/all').pipe(
      map(productTypes => productTypes.map(productType => { return {  ...productType, ...{ parentTitle: productType.parent_sku?.title, sku: productType._id } } as ProductTypeInterface }) )
    )
  }

  getProductTypeByID(sku: String): Observable<ProductTypeInterface> {
    return this.httpClient.get<any>(`http://172.19.15.128:30001/product/type/${sku}`)
  }

  createProductType(productType: ProductTypeInterface) {
    return this.httpClient.post<ProductTypeInterface>('http://172.19.15.128:30001/product/type', productType)
  }

  updateProductType(productType: ProductTypeInterface) {
    return this.httpClient.put<ProductTypeInterface>('http://172.19.15.128:30001/product/type', productType)
  }

  deleteProductTypeByID(sku: String): Observable<any> {
    return this.httpClient.delete('http://172.19.15.128:30001/product/type', { body: { sku } })
  }
}
