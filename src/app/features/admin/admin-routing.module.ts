import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHubComponent } from './components/admin-hub/admin-hub.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductsComponent } from './components/products/products.component';

const routes: Routes = [
  { path: 'admin', component: AdminHubComponent,
    children: [
      { path: '', component: ProductsComponent },
      { path: 'product', component: ProductFormComponent },
      { path: 'product/:id', component: ProductFormComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
