export interface CartItemInterface {
    sku: String,
    price: Number,
    quantity: Number
}

export interface CartInterface {
    email: String,
    cart_items: CartItemInterface[]
}