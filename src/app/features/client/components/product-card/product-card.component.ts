import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { ItemInterface, ProductTypeInterface } from 'src/app/shared/interfaces/product';
import { ProductService } from 'src/app/shared/services/product.service';
import { StateService } from 'src/app/shared/services/state.service';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { CartItemInterface } from '../../interfaces/cart';
import { NotificationService } from '../../services/notification.service';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent extends SubscribeHelperComponent implements OnInit {
  public productTypes: ProductTypeInterface[] = []
  private cartItems: CartItemInterface[] = []
  public items: ItemInterface[] = []

  constructor(
    private productService: ProductService,
    private orderService: OrderService,
    private router: Router,
    private snackBar: MatSnackBar,
    private notificationService: NotificationService,
    private authService: AuthService,
    private stateService: StateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.addSubscription(this.productService.getProductTypes().subscribe(
      (productTypes) => {
        this.productTypes = productTypes
      }
    ))

    this.addSubscription(this.orderService.getCart().subscribe(
      (cart) => {
        this.cartItems = cart.cart_items
      }
    ))

    this.addSubscription(this.stateService.items.subscribe(
      (items) => {
        this.items = items
      }
    ))
  }

  buyOne(sku: String | undefined, price: Number | undefined): void {
    let cartItem = {
      sku
    } as CartItemInterface

    const entry = this.cartItems?.find(cI => cI.sku === sku)
    if (entry) {
      cartItem = { ...cartItem, ...{ quantity: Number(entry.quantity) + 1 } }
    } else {
      cartItem = { ...cartItem, ...{ quantity: 1, price: Number(price) } }
    }

    this.addSubscription(this.orderService.updateCartSingle(cartItem).subscribe(
      () => {
        this.snackBar.open('Item added to the cart.', undefined, { duration: 1500 })

        this.addSubscription(this.productService.getProductTypes().subscribe(
          (productTypes) => {
            this.productTypes = productTypes
          }
        ))

        this.addSubscription(this.orderService.getCart().subscribe(
          (cart) => {
            this.cartItems = cart.cart_items
          }
        ))
      },
      (error) => {
        const message = (error?.error?.error || error?.message) || 'Something went wrong!'
        this.snackBar.open(message, undefined, { duration: 2500 })
      }
    ))
  }

  subscribeNotification(sku: String | undefined): void {
    const email = this.authService.getUser().email
    this.addSubscription(this.notificationService.subscribe(email, String(sku)).subscribe(
      () => {
        this.snackBar.open('You will receive an email when this product will be on stock.', undefined, { duration: 2500 })
      },
      (error) => {
        if (Number(error.status)) {
          this.snackBar.open('You are already subscribed.', undefined, { duration: 2500 })
        }
      }
    ))
  }

  gather(sku: String | undefined, description: String, price: Number): void {
    const item = this.items.find(i => i.sku === sku)
    if (item) {
      item.quantity = Number(item.quantity) + 1
    } else {
      this.items.push({ sku, description, price, quantity: 1 } as ItemInterface) 
    }

    this.stateService.setItems(this.items)
  }

  isGatherable(sku: String | undefined): boolean {
    const item = this.items.find(i => i.sku === sku)
    const entry = this.productTypes.find(pT => pT.sku === sku)

    if (!item) {
      return true
    }

    return item.quantity < Number(entry?.stock)
  }
}
