import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ItemInterface } from 'src/app/shared/interfaces/product';
import { StateService } from 'src/app/shared/services/state.service';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { CartItemInterface } from '../../interfaces/cart';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-gather',
  templateUrl: './gather.component.html',
  styleUrls: ['./gather.component.scss']
})
export class GatherComponent extends SubscribeHelperComponent implements OnInit {
  public items: ItemInterface[] = []
  private cartItems: CartItemInterface[] = []

  constructor(
    private stateService: StateService,
    private orderService: OrderService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnInit(): void {
    this.addSubscription(this.stateService.items.subscribe(
      (items) => {
        this.items = items
      }
    ))

    this.addSubscription(this.orderService.getCart().subscribe(
      (cart) => {
        this.cartItems = cart.cart_items
      }
    ))
  }

  onMoveToCart(): void {
    const cartItems: CartItemInterface[] = []
    this.items.forEach(item => {
      const entry = this.cartItems.find(cI => cI.sku === item.sku)
      cartItems.push({ sku: item.sku, price: item.price, quantity: Number(item.quantity) + (entry ? Number(entry.quantity) : 0) } as CartItemInterface)
    })

    this.addSubscription(this.orderService.updateCartMultiple(cartItems).subscribe(
      () => {
        this.stateService.setItems([])
        this.snackBar.open('Gathered items added to the cart.', undefined, { duration: 1500 })
      },
      (error) => {
        const message = (error?.error?.error || error?.message) || 'Something went wrong!'
        this.snackBar.open(message, undefined, { duration: 2500 })
      }
    ))
  }

}
