import { Component, OnInit } from '@angular/core';
import { UserInterface } from 'src/app/core/interfaces/auth';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-client-hub',
  templateUrl: './client-hub.component.html',
  styleUrls: ['./client-hub.component.scss']
})
export class ClientHubComponent implements OnInit {
  public user: UserInterface = {} as UserInterface
  public isClientSidenavOpened = false

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.user = this.authService.getUser()
  }

  toggleSidenav(): void {
    this.isClientSidenavOpened = !this.isClientSidenavOpened
  }
}
