import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTable } from '@angular/material/table';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { CartItemInterface } from '../../interfaces/cart';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent extends SubscribeHelperComponent implements OnInit {
  public tableData: CartItemInterface[] = []
  public displayedColumns = ['sku', 'price', 'quantity', 'actions']

  @ViewChild(MatTable) table: MatTable<CartItemInterface> | undefined;

  constructor(
    private orderService: OrderService,
    private matSnackbar: MatSnackBar
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadCartItems()
  }

  modifyQuantity(cartItem: CartItemInterface, difference: Number): void {
    cartItem = { ...cartItem, quantity: Number(cartItem.quantity) + Number(difference) }

    if (cartItem.quantity === 0) {
      return this.deleteEntry(cartItem.sku)
    }

    this.addSubscription(this.orderService.updateCartSingle(cartItem).subscribe(
      () => {
        this.loadCartItems()
      },
      (error) => {
        const message = (error?.error?.error || error?.message) || 'Something went wrong!'
        this.matSnackbar.open(message, undefined, { duration: 2500 })
      }
    ))
  }

  deleteEntry(sku: String): void {
    this.addSubscription(this.orderService.deleteCartItem(sku).subscribe(
      () => {
        this.loadCartItems()
      }
    ))
  }

  placeOrder(): void {
    this.addSubscription(this.orderService.createOrder().subscribe(
      () => {
        this.matSnackbar.open('Order placed successfully', undefined, { duration: 2500 })
        this.loadCartItems()
      }
    ))
  }

  loadCartItems(): void {
    this.addSubscription(this.orderService.getCart().subscribe(
      (cart) => {
        this.tableData = cart.cart_items ? cart.cart_items : []
        this.table?.renderRows()
      }
    ))
  }
}
