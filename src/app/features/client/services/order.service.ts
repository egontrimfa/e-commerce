import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { CartInterface, CartItemInterface } from '../interfaces/cart';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private bearerToken: string | null | undefined = null 

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {
    this.bearerToken = `Bearer ${this.authService.getAccessToken()}`
  }

  getCart(): Observable<CartInterface> {
    return this.httpClient.get<CartInterface>('http://172.19.15.128:30003/cart', {
      headers: new HttpHeaders({
        Authorization: String(this.bearerToken)
      })
    })
  }

  updateCartSingle(cartItem: CartItemInterface): Observable<any> {
    return this.httpClient.put<any>('http://172.19.15.128:30003/cart/item', { items: [ cartItem ] }, {
      headers: new HttpHeaders({
        Authorization: String(this.bearerToken)
      })
    })
  }

  updateCartMultiple(cartItems: CartItemInterface[]): Observable<any> {
    return this.httpClient.put<any>('http://172.19.15.128:30003/cart/item', { items: cartItems }, {
      headers: new HttpHeaders({
        Authorization: String(this.bearerToken)
      })
    })
  }

  deleteCartItem(sku: String): Observable<any> {
    return this.httpClient.delete<any>('http://172.19.15.128:30003/cart/item', {
      body: { sku },
      headers: new HttpHeaders({
        Authorization: String(this.bearerToken)
      })
    })
  }

  createOrder(): Observable<any> {
    return this.httpClient.post<any>('http://172.19.15.128:30003/order', null, {
      headers: new HttpHeaders({
        Authorization: String(this.bearerToken)
      })
    })
  }
}
