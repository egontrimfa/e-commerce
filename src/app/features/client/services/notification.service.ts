import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(
    private httpClient: HttpClient
  ) { }

  subscribe(email: String, sku: String): Observable<any> {
    return this.httpClient.post<any>('http://172.19.15.128:30002/subscribe', { email, sku })
  }
}
