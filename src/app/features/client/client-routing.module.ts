import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { ClientHubComponent } from './components/client-hub/client-hub.component';
import { ProductCardComponent } from './components/product-card/product-card.component';

const routes: Routes = [
  { path: 'client', component: ClientHubComponent,
    children: [
      { path: '', component: ProductCardComponent },
      { path: 'cart', component: CartComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
