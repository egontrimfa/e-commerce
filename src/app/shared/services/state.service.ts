import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ItemInterface } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  public items: BehaviorSubject<ItemInterface[]> = new BehaviorSubject<ItemInterface[]>([])

  constructor() { }

  setItems(items: ItemInterface[]): void {
    this.items.next(items)
  }
}
