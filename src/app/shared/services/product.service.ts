import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { ProductTypeInterface } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getProductTypes(): Observable<ProductTypeInterface[]> {
    return this.httpClient.get<any[]>('http://172.19.15.128:30001/product/type/list/all').pipe(
      map(productTypes => productTypes.map(productType => { return {  ...productType, ...{ parentTitle: productType.parent_sku?.title, sku: productType._id } } as ProductTypeInterface }) )
    )
  }
}
