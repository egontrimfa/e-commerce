export interface ProductInterface {
  parentSKU: String,
  title: String
}

export interface PriceInterface {
  original: Number,
  discount: Number,
  currency: String
}

export interface DimensionInterface {
  width: Number,
  height: Number,
  length: Number,
  unit: String
}

export interface ImageInterface {
  source: String,
  label: String
}

export interface ProductTypeInterface {
  sku?: String,
  parent_sku?: String,
  parentTitle?: String,
  available: Boolean,
  stock: Number,
  prices: PriceInterface[],
  description: String,
  dimension: DimensionInterface,
  images: ImageInterface[]
}

export interface ProductTypeTableInterface {
  sku: String,
  parentTitle: String,
  description: String,
  stock: Number,
  available: Boolean
}

export interface ItemInterface {
  sku: String,
  price: Number,
  description: String,
  quantity: Number
}
