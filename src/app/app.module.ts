import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './core/components/login/login.component';
import { AdminModule } from './features/admin/admin.module';
import { ClientModule } from './features/client/client.module';
import { SubscribeHelperComponent } from './utils/subscribe-helper/subscribe-helper.component';
import { RegisterComponent } from './core/components/register/register.component';
import { WelcomeComponent } from './core/components/welcome/welcome.component';
import { RecoverComponent } from './core/components/recover/recover.component';
import { MainComponent } from './core/components/main/main.component';
import { ResetComponent } from './core/components/reset/reset.component';

@NgModule({
  declarations: [
    MainComponent,
    LoginComponent,
    SubscribeHelperComponent,
    RegisterComponent,
    WelcomeComponent,
    RecoverComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    HttpClientModule,
    AdminModule,
    ClientModule
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class AppModule { }
