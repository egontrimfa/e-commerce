import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './core/components/login/login.component';
import { RecoverComponent } from './core/components/recover/recover.component';
import { RegisterComponent } from './core/components/register/register.component';
import { ResetComponent } from './core/components/reset/reset.component';
import { WelcomeComponent } from './core/components/welcome/welcome.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent,
    children: [
      { path: '', component: LoginComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'recover', component: RecoverComponent },
      { path: 'reset', component: ResetComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
