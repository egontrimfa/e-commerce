import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import JWTDecode from 'jwt-decode';
import { AccessTokenInterface, DecodedJWTInterface, UserInterface } from '../interfaces/auth';
import { CONSTANTS } from 'src/app/utils/constant';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService implements OnInit {
  private currentUser: UserInterface = {} as UserInterface

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    this.setCurrentUser()
  }

  login(email: String, password: String): Observable<AccessTokenInterface> {
    return this.httpClient.post<AccessTokenInterface>('http://172.19.15.128:30000/users/login', { email, password })
  }

  register(fullName: String, email: String, password: String): Observable<any> {
    return this.httpClient.post<any>('http://172.19.15.128:30000/users/signup', { fullName, email, password })
  }

  isLoggedIn(): Boolean {
    return this.currentUser == null ? false : true
  }

  verify(verificationToken: String): Observable<any> {
    return this.httpClient.get<any>(`http://172.19.15.128:30000/users/verify/${ verificationToken }`)
  }

  recover(email: String): Observable<any> {
    return this.httpClient.post<any>('http://172.19.15.128:30000/users/recover', { email })
  }

  reset(resetToken: String, password: String): Observable<any> {
    return this.httpClient.post<any>('http://172.19.15.128:30000/users/reset', { token: resetToken, password })
  }

  getUser(): UserInterface {
    if (Object.entries(this.currentUser).length === 0) {
      this.setCurrentUser()
    }

    return this.currentUser
  }

  getAccessToken() {
    if(!this.isLoggedIn()) {
      return
    }

    return localStorage.getItem(CONSTANTS.ACCESS_TOKEN)
  }

  setCurrentUser() {
    const accessToken = localStorage.getItem(CONSTANTS.ACCESS_TOKEN)

    if (accessToken) {
      this.currentUser = (JWTDecode(accessToken) as DecodedJWTInterface).user
    }
  }
}
