import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.scss']
})
export class RecoverComponent extends SubscribeHelperComponent {
  public formGroup: FormGroup = {} as FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    super()

    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    })
  }

  // Getters
  get email(): AbstractControl | null { return this.formGroup.get('email') }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return
    }

    this.addSubscription(this.authService.recover(this.email?.value).subscribe(
      (response) => {
        this.snackBar.open(response?.message, undefined, { duration: 2500 })
        setInterval(() => {
          this.router.navigate(['/login'])
        }, 3000)
      }
    ))
  }
}
