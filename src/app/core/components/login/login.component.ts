import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CONSTANTS } from 'src/app/utils/constant';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { AccessTokenInterface } from '../../interfaces/auth';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends SubscribeHelperComponent {
  public formGroup: FormGroup = {} as FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    super()

    localStorage.removeItem(CONSTANTS.ACCESS_TOKEN)

    this.formGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  // Getters
  get email(): AbstractControl | null { return this.formGroup.get('email') }
  get password(): AbstractControl | null { return this.formGroup.get('password') }

  onSubmit(): any {
    if (this.formGroup.invalid) {
      return
    }
    // const access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiZWdvbnRyaW1mYUBnbWFpbC5jb20iLCJlbWFpbFZlcmlmaWVkIjp0cnVlLCJmdWxsTmFtZSI6IkVnb24gVHJpbWZhIn19.1hLcDp3_ltA_8Fq3a_HakGhVKQF5eSl3qG4E-l3MT-A"
    // localStorage.setItem(CONSTANTS.ACCESS_TOKEN, access_token)
    // this.router.navigate(['/admin'])

    const loginObserver: Observable<any> = this.authService.login(this.email?.value, this.password?.value)
    this.addSubscription(loginObserver.subscribe(
      (response: AccessTokenInterface) => {
        if (response && response.accessToken) {
          localStorage.setItem(CONSTANTS.ACCESS_TOKEN, String(response.accessToken))
          this.router.navigate(['/admin'])
        }
      },
      (error: any) => {
        const message = (error?.error?.error || error?.message) || 'Something went wrong!'
        this.snackBar.open(message, undefined, { duration: 2500 })
      }
    ))
  }
}
