import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent extends SubscribeHelperComponent implements OnInit {
  public formGroup: FormGroup = {} as FormGroup
  private resetToken: String | null = null

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    super()

    this.formGroup = this.formBuilder.group({
      password: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    if (this.route.snapshot.queryParamMap.has('reset-token')) {
      this.resetToken = this.route.snapshot.queryParamMap.get('reset-token')
    }
  }

  // Getters
  get password(): AbstractControl | null { return this.formGroup.get('password') }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return
    }

    this.addSubscription(this.authService.reset(String(this.resetToken), this.password?.value).subscribe(
      (response) => {
        this.snackBar.open(response?.message, undefined, { duration: 2500 })
        setInterval(() => {
          this.router.navigate(['/login'])
        }, 3000)
      }
    ))
  }
}
