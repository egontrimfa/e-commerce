import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends SubscribeHelperComponent {
  public formGroup: FormGroup = {} as FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    super();

    this.formGroup = this.formBuilder.group({
      fullName: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(64)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(64)]]
    })
  }

  // Getters
  get fullName(): AbstractControl | null { return this.formGroup.get('fullName') }
  get email(): AbstractControl | null { return this.formGroup.get('email') }
  get password(): AbstractControl | null { return this.formGroup.get('password') }

  onSubmit(): any {
    if (this.formGroup.invalid) {
      return
    }

    this.addSubscription(this.authService.register(this.fullName?.value, this.email?.value, this.password?.value).subscribe(
      () => {
        this.snackBar.open('You have successfully registered. Check your e-mail address to verify the registration.', undefined, { duration: 2500 })
        setInterval(() => {
          this.router.navigate(['/login'])
        }, 3000)
      }
    ))
  }

}
