import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { SubscribeHelperComponent } from 'src/app/utils/subscribe-helper/subscribe-helper.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent extends SubscribeHelperComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.route.snapshot.queryParamMap.has('verification-token')) {
      const verificationToken = this.route.snapshot.queryParamMap.get('verification-token')
      this.sendVerification(verificationToken)
    }
  }

  sendVerification(verificationToken: String | null): void {
    this.addSubscription(this.authService.verify(String(verificationToken)).subscribe(
      (response) => {
        this.snackBar.open(response?.message, undefined, { duration: 2500 })
      }
    ))
  }
}
