export interface UserInterface {
    email: String,
    emailVerified: Boolean,
    fullName: String
}

export interface AccessTokenInterface {
    accessToken: String
}

export interface DecodedJWTInterface {
    iat: Number,
    user: UserInterface
}