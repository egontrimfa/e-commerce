import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-subscribe-helper',
  templateUrl: './subscribe-helper.component.html',
  styleUrls: ['./subscribe-helper.component.scss']
})
export class SubscribeHelperComponent implements OnDestroy {
  private subscriptions: Subscription[] = [];

  addSubscription(subscription: Subscription): void {
    this.subscriptions.push(subscription);
  }

  clearSubscriptions(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngOnDestroy(): void {
    this.clearSubscriptions();
  }
}
